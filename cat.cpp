///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Christopher Agcanas <agcanas8@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   05 May 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

//CatEmpire
bool CatEmpire::empty(){
   return (topCat == nullptr);
}//empty()


void CatEmpire::addCat( Cat* newCat){
   //If empty, set newCat as root. 
   if(topCat == nullptr){
      topCat = newCat;
      return;
   }else{//List is not empty
      addCat(topCat, newCat);
   }
}//public addCat()

void CatEmpire::addCat(Cat* atCat, Cat* newCat){
   //Sorting by name
   //If atCat is greater than newCat
   if(atCat->name > newCat->name){
      //Look at left of node
      if(atCat->left == nullptr){
         atCat->left = newCat;
      }else{
         //Move down the tree
         addCat(atCat->left, newCat);
      }
   }
   //If atCat is less than newCat
   if(atCat->name < newCat->name){
      //Look at right of node
      if(atCat->right == nullptr){
         atCat->right = newCat;
      }else{
         //Move down tree
         addCat(atCat->right, newCat);
      }
   }

   return;
}//private addCat

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse(Cat* atCat, int depth) const{
   //DFS Reverse Inorder: Right, Parent, Left
   if(atCat == nullptr)
      return;

   //Go to right sub tree
   dfsInorderReverse(atCat->right, depth+1);
   
   //Print
   cout << string(6 * (depth-1), ' ') << atCat->name;

   //This is a leaf node
   if(atCat-> left == nullptr && atCat->right == nullptr){
      cout << endl;
   }
   
   //There is both a right and left node
   if(atCat->left != nullptr && atCat->right != nullptr){
      cout << "<" << endl;
   }

   //There is right node but there is a lest node
   if(atCat->left != nullptr && atCat->right == nullptr){
      //Problem with forward slash
      cout << "\\" << endl;
   }
   
   //Theree is no left node but there is a right node
   if(atCat->left == nullptr && atCat->right != nullptr){
      cout << "/" << endl;
   }

   //Go to left sub tree
   dfsInorderReverse(atCat->left, depth+1);

}//dfInorderReverse

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}


void CatEmpire::dfsInorder(Cat* atCat) const{
   if(atCat == nullptr)
      return;
   
   //Look at left subtree
   dfsInorder(atCat->left);

   cout << atCat->name << endl;

   //Look at right subtree
   dfsInorder(atCat->right);

}//dfsInorder 


void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder(Cat* atCat) const {
   if(atCat == nullptr)
      return;

   //Print
   if(atCat->left != nullptr && atCat->right != nullptr){
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }

   if(atCat->left != nullptr && atCat->right == nullptr){
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }

   if(atCat->left == nullptr && atCat->right != nullptr){
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }

   //Look at left subtree
   dfsPreorder(atCat->left);
   //Look at right subtree
   dfsPreorder(atCat->right);


}//dfPreorder

void CatEmpire::catGenerations() const{
   int gen = 1;

   queue<Cat*> catQueue;
   //catQueue.push(topCat);
   catQueue.push(nullptr);
   catQueue.push(topCat);
   
   //Using null to track the depth mean there will always be one enry
   while(catQueue.size() > 1){
   //while(catQueue.empty()){
      //Look at the cat in the front of list
      Cat* cat = catQueue.front();
      catQueue.pop();

      //On a new generation
      if(cat == nullptr){ 
         //Print Generation
         if(gen != 1)
            cout << endl;
         getEnglishSuffix(gen);
         cout << " Generation" << endl;
         //Start prinnt cat name
         cout << "  ";
         gen++;
         //Place marker for start of next generation
         catQueue.push(nullptr);
         continue;
      }

      cout << cat->name << " ";

      if(cat == nullptr)
         return;

      //look at the left child first
      if(cat->left != nullptr){
         catQueue.push(cat->left);
      }

      //Look at right child latter
      if(cat->right != nullptr){
         catQueue.push(cat->right);
      }
   }
   cout << endl;

   //Emptying queue
   while(!catQueue.empty()){
      catQueue.pop();//Should only run once
   }

}//catGeneration

void CatEmpire::getEnglishSuffix(int n) const{
   /*ends in 0 = -th, 1 = -st, 2 = -nd, 3 = -rd, 4-9 = -th
    * Exception 11, 12, 13 use -th
   */

   //Looking at last 2 digits
   if(n%100 < 11 || n%100 > 13){
      //Looking at last digit
      switch(n%10){
         case 1: 
            cout << n << "st";
            break;
         case 2:
            cout << n <<  "nd";
            break;
         case 3:
            cout << n <<  "rd";
            break;
         default:
            cout << n <<  "th";
            break;
      }
   }else{
      cout << n <<  "th";
   }

}//getEnglishSuffix

